-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema GepardTest
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema GepardTest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `GepardTest` DEFAULT CHARACTER SET utf8 ;
USE `GepardTest`;

-- -----------------------------------------------------
-- Table `GepardTest`.`Estados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GepardTest`.`Estados` (
  `idEstado` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`idEstado`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GepardTest`.`Cidades`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GepardTest`.`Cidades` (
  `idCidade` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `idEstado` INT NOT NULL,
  PRIMARY KEY (`idCidade`),
  CONSTRAINT `fk_Cidades_Estados1`
    FOREIGN KEY (`idEstado`)
    REFERENCES `GepardTest`.`Estados` (`idEstado`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `GepardTest`.`Endereco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GepardTest`.`Endereco` (
  `idEndereco` INT(11) NOT NULL AUTO_INCREMENT,
  `rua` VARCHAR(70) NOT NULL,
  `numero` VARCHAR(40) NOT NULL,
  `complemento` VARCHAR(50) NULL DEFAULT NULL,
  `estado` VARCHAR(50) NOT NULL,
  `cidade` VARCHAR(50) NOT NULL,
  `cep` VARCHAR(15) NOT NULL,
  `pais` VARCHAR(40) NOT NULL,
  `bairro` VARCHAR(100) NOT NULL,
  `idCidade` INT NOT NULL,
  PRIMARY KEY (`idEndereco`),
  CONSTRAINT `fk_Endereco_Cidades1`
    FOREIGN KEY (`idCidade`)
    REFERENCES `GepardTest`.`Cidades` (`idCidade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `GepardTest`.`RedeSocial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GepardTest`.`RedeSocial` (
  `idRedeSocial` INT(11) NOT NULL AUTO_INCREMENT,
  `facebook` VARCHAR(100) UNIQUE NULL DEFAULT NULL,
  `instagram` VARCHAR(100) UNIQUE NULL DEFAULT NULL,
  `whatsapp` INT(11) UNIQUE NOT NULL,
  `twitter` VARCHAR(100) UNIQUE NULL DEFAULT NULL,
  PRIMARY KEY (`idRedeSocial`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `GepardTest`.`Treinador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GepardTest`.`Treinador` (
  `idTreinador` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(200) NOT NULL,
  `email` VARCHAR(200) UNIQUE NOT NULL,
  `senha` VARCHAR(200) NOT NULL,
  `cref` VARCHAR(10) UNIQUE NOT NULL,
  `formacao` VARCHAR(100) NOT NULL,
  `descricao` LONGTEXT NULL DEFAULT NULL,
  `dataNascimento` DATETIME NOT NULL,
  `dadosBancarios` VARCHAR(50) NULL DEFAULT NULL,
  `dataCadastro` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sexo` VARCHAR(45) NOT NULL,
  `idEndereco` INT(11) NOT NULL,
  PRIMARY KEY (`idTreinador`),
  CONSTRAINT `fk_Treinador_Endereco1`
    FOREIGN KEY (`idEndereco`)
    REFERENCES `GepardTest`.`Endereco` (`idEndereco`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `GepardTest`.`Treino`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GepardTest`.`Treino` (
  `idTreino` INT(11) NOT NULL AUTO_INCREMENT,
  `frequencia` VARCHAR(100) NOT NULL,
  `descricao` LONGTEXT NOT NULL,
  `carga` INT(11) NULL DEFAULT NULL,
  `duracao` TIME NOT NULL,
  `tipo` VARCHAR(100) NOT NULL,
  `data` DATETIME NOT NULL,
  PRIMARY KEY (`idTreino`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `GepardTest`.`Usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GepardTest`.`Usuarios` (
  `id_usuario` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) UNIQUE NOT NULL,
  `email` VARCHAR(200) UNIQUE NOT NULL,
  `username` VARCHAR(40) NOT NULL,
  `senha` VARCHAR(100) NOT NULL,
  `nascimento` DATE NOT NULL,
  `medidas_corporais` DECIMAL(3,2) NULL DEFAULT NULL,
  `altura` DECIMAL(2,2) NULL DEFAULT NULL,
  `sexo` VARCHAR(20) NOT NULL,
  `peso` DECIMAL(3,1) NULL DEFAULT NULL,
  `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `idRedeSocial` INT(11) NULL,
  `idEndereco` INT(11) NOT NULL,
  PRIMARY KEY (`id_usuario`),
  CONSTRAINT `fk_usuarios_Endereco1`
    FOREIGN KEY (`idEndereco`)
    REFERENCES `GepardTest`.`Endereco` (`idEndereco`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_RedeSocial1`
    FOREIGN KEY (`idRedeSocial`)
    REFERENCES `GepardTest`.`RedeSocial` (`idRedeSocial`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `GepardTest`.`Usuarios_has_Treinador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `GepardTest`.`Usuarios_has_Treinador` (
  `Usuarios_id_usuario` INT(11) NOT NULL,
  `Treinador_idTreinador` INT(11) NOT NULL,
  `Treino_idTreino` INT(11) NULL,
  `eTreinador` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`Usuarios_id_usuario`, `Treinador_idTreinador`),
  CONSTRAINT `fk_Usuarios_has_Treinador_Usuarios1`
    FOREIGN KEY (`Usuarios_id_usuario`)
    REFERENCES `GepardTest`.`Usuarios` (`id_usuario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuarios_has_Treinador_Treinador1`
    FOREIGN KEY (`Treinador_idTreinador`)
    REFERENCES `GepardTest`.`Treinador` (`idTreinador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuarios_has_Treinador_Treino1`
    FOREIGN KEY (`Treino_idTreino`)
    REFERENCES `GepardTest`.`Treino` (`idTreino`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
