const http = require('http');
const app = require('./app/app');

const port = process.env.port || 4230;
const server = http.createServer(app);

server.listen(port, '0.0.0.0', () => {
    console.log("Starting");
});
