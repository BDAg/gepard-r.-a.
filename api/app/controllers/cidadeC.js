const dbPool = require('../config/database');

exports.find = (req, res, next) => {

    dbPool.getConnection((err, conn) => {
        if (err) {
            conn.release();
            res.status(500).json({
                "response": null,
                "error": err
            });
        } else {
            conn.query('select * from Cidades', (err, results) => {
                if (err) {
                    conn.release();
                    res.status(500).json({
                        "response": null,
                        "error": err
                    });
                } else {
                    conn.release();
                    res.status(200).json({
                        "response": results,
                        "error": null
                    });
                }
            })
        }
    });

}