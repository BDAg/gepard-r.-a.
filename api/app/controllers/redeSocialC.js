const dbPool = require('../config/database');

exports.inserir = function (req, res) {
    let facebook = req.body.Facebook;
    let instagram = req.body.Instagram;
    let whatsApp = req.body.WhatsApp;
    let email = req.body.Email;
    let twitter = req.body.Twitter;
    
    if (!facebook) {
        return res.status(400).send({ error:true, message: 'Insira as redes sociais do treinador' });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.end();
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {
                conn.query("INSERT INTO RedeSocial SET ? ", { Facebook:facebook, Instagram : instagram,WhatsApp:whatsApp, Email:email, Twitter:twitter}, function (error, results, fields) {
                    if (err) {
                        conn.end();
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        conn.end();
                        res.status(200).json({
                            error: false,
                            data: results,
                            message: 'redeSocial inserida com sucesso!'
                        });
                    }
                });
            }
        });
    }
};

exports.editar = function (req, res) {

    let id_redeSocial = req.params.id;
    let facebook = req.body.Facebook;
    let instagram = req.body.Instagram;
    let whatsApp = req.body.WhatsApp;
    let email = req.body.Email;
    let twitter = req.body.Twitter;

    if (!facebook) {
        return res.status(400).send({ error: facebook, message: 'Insira as redes sociais e /id pela rota.' });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.end();
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {
                conn.query("UPDATE RedeSocial SET ? WHERE Id_redeSocial= ?", [{Facebook:facebook, Instagram : instagram,WhatsApp:whatsApp, Email:email, Twitter:twitter},id_redeSocial], function (error, results, fields) {
                    if (err) {
                        conn.end();
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        conn.end();
                        res.status(200).json({
                            error: false,
                            data: results,
                            message: 'redeSocial atualizada com sucesso!'
                        });
                    }
                });
            }
        });
    }
};

exports.deletar = function (req, res) {
  
  let id_redeSocial = req.body.Id_redeSocial;

  if (!id_redeSocial) {
      return res.status(400).send({ error: true, message: 'insira Id_redeSocial' });
  } else {
    dbPool.getConnection((err, conn) => {
        if (err) {
            conn.end();
            res.status(500).json({
                "response": null,
                "error": err
            });
        } else {
            conn.query('DELETE FROM RedeSocial WHERE Id_redeSocial = ?', [id_redeSocial], function (error, results, fields) {
                if (err) {
                    conn.end();
                    res.status(500).json({
                        "response": null,
                        "error": err
                    });
                } else {
                    conn.end();
                    res.status(200).json({
                        error: false,
                        data: results,
                        message: 'redeSocial deletada com sucesso!'
                    });
                }
            });
        }
    });
  }
};