const dbPool = require('../config/database');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.buscarUsuarios = (req, res, next) => {
    const listaUsuarios = `SELECT *
                           FROM
                           Usuarios`;
    dbPool.getConnection((err, conn) => {
        if (err) {
            res.status(500).json({
                "response": null,
                "error": err
            });
        } else {
            conn.query(listaUsuarios, (err, results, fields) => {
                if (err) {
                    conn.release();
                    res.status(500).json({
                        error: err,
                        message: "Erro ao conectar banco."
                    });
                } else {
                    conn.release();
                    res.status(200).send({
                        error: false,
                        data: results,
                        message: "Lista de usuários!"
                    });
                }
            });
        }
    });
}

exports.insert = function (req, res) {
    const {
        nome,
        senha,
        email
    } = req.body;

    if (!nome || !senha || !email) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {

        dbPool.getConnection((err, conn) => {
            if (err) {
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {
                const novoTreinador = `
                    insert into Usuarios
                    (nome,
                    email,
                    senha)
                    values
                    ( ?, ?, ? );
                `;

                bcrypt.hash(senha, 10, (err, hash) => {
                    conn.query(novoTreinador,
                        [nome, email, hash],
                        (err, results, fields) => {
                            if (err) {
                                conn.release();
                                res.status(500).json({
                                    error: err,
                                    message: "Erro ao conectar banco."
                                });
                            } else {
                                conn.release();
                                res.status(201).json({
                                    error: null,
                                    message: 'Usuario registrado com sucesso!'
                                })
                            }
                        })
                });
            }
        });

    }
}

exports.update = function (req, res) {

    let {
        nome,
        username,
        altura,
        peso,
        dataNascimento,
        sexo,
        rua,
        numero,
        complemento,
        imagem,
        cep,
        bairro,
        idCidade,
        token
    } = req.body;

    if (!nome || !sexo || !dataNascimento || !rua || !numero || !complemento ||
        !bairro || !idCidade || !token || !cep) {
        return res.status(400).send({
            error: true,
            message: 'Invalid Params!'
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.release();
                res.status(500).json({
                    error: err,
                    message: 'Erro ao cadastrar.'
                });
            } else {

                if (!imagem) {
                    imagem = "";
                }

                let tokenDecoded = jwt.verify(token, '2fiit');

                const buscarTreinador = `
                    select * from Usuarios where id_usuario = ?
                `;

                conn.query(buscarTreinador, [tokenDecoded['id_usuario']], (err, result, fields) => {
                    if (err) {
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        if (result.length < 1) {
                            res.status(500).json({
                                "response": null,
                                "error": "Not Found"
                            });
                        } else {
                            console.log(result[0].idEndereco);
                            if (result[0].idEndereco == null) {
                                const novoEndereco = `
                                insert into Endereco
                                (rua,
                                numero,
                                complemento,
                                bairro,
                                idCidade)
                                values
                                ( ?, ?, ?, ?, ? );
                                `;

                                conn.query(novoEndereco, [rua, numero, complemento, bairro, idCidade], (err, result) => {
                                    if (err) {
                                        res.status(500).json({
                                            "response": null,
                                            "error": err
                                        });
                                    } else {
                                        const novoTreinador = `
                                            UPDATE Usuarios
                                            SET
                                                nome = ?,
                                                imagem = ?,
                                                username = ?,
                                                nascimento = ?,
                                                altura = ?,
                                                peso = ?,
                                                idEndereco = ?,
                                                sexo = ?
                                            WHERE 
                                                id_usuario = ?
                                        `;

                                        conn.query(novoTreinador,
                                            [nome, imagem, username, dataNascimento, altura, peso, result.insertId, sexo, tokenDecoded['id_usuario']],
                                            (err, results, fields) => {
                                            if (err) {
                                                conn.release();
                                                res.status(500).json({
                                                    error: err,
                                                    message: "Erro ao conectar banco."
                                                });
                                            } else {
                                                conn.release();
                                                res.status(201).json({
                                                    error: null,
                                                    message: 'Usuario atualizado com sucesso!'
                                                })
                                            }
                                        })
                                    }
                                });
                            } else {

                                const novoEndereco = `
                                    UPDATE Endereco
                                    SET
                                        rua = ?,
                                        numero = ?,
                                        complemento = ?,
                                        cep = ?,
                                        bairro = ?,
                                        idCidade = ?
                                    WHERE
                                        idEndereco = ?;
                                `;

                                conn.query(novoEndereco, [rua, numero, complemento, cep, bairro, idCidade, result[0].idEndereco], (err, results) => {
                                    if (err) {
                                        res.status(500).json({
                                            "response": null,
                                            "error": err
                                        });
                                    } else {
                                        const novoTreinador = `
                                            UPDATE Usuarios
                                            SET
                                                nome = ?,
                                                imagem = ?,
                                                username = ?,
                                                nascimento = ?,
                                                altura = ?,
                                                peso = ?,
                                                idEndereco = ?,
                                                sexo = ?
                                            WHERE 
                                                id_usuario = ?
                                        `;

                                        conn.query(novoTreinador,
                                            [nome, imagem, username, dataNascimento, altura, peso, result.insertId, sexo, tokenDecoded['id_usuario']],
                                            (err, results, fields) => {
                                            if (err) {
                                                conn.release();
                                                res.status(500).json({
                                                    error: err,
                                                    message: "Erro ao conectar banco."
                                                });
                                            } else {
                                                conn.release();
                                                res.status(201).json({
                                                    error: null,
                                                    message: 'Usuario atualizado com sucesso!'
                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        }
                    }
                });
            }
        })
    }
};

exports.cadastrarUsuario = (req, res, next) => {

    const {
        nome,
        username,
        email,
        senha,
        nascimento,
        sexo,
        rua,
        numero,
        complemento,
        bairro,
        idCidade
    } = req.body;

    if (!nome || !username || !email || !senha || !nascimento || !sexo || !rua ||
        !numero || !complemento || !bairro || !idCidade) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.release();
                res.status(500).json({
                    error: err,
                    message: 'Erro ao cadastrar.'
                });
            } else {

                const novoEndereco = `
                    insert into Endereco
                    (rua,
                    numero,
                    complemento,
                    bairro,
                    idCidade)
                    values
                    ( ?, ?, ?, ?, ? );
                `;

                conn.query(novoEndereco, [rua, numero, complemento, bairro, idCidade], (err, result) => {
                    if (err) {
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        const novoUsuario = `
                            insert into
                            Usuarios (nome, username, email, senha, nascimento, sexo, idEndereco)
                            values ( ?, ?, ?, ?, ?, ?, ? )
                        `;

                        bcrypt.hash(senha, 10, (err, hash) => {
                            conn.query(novoUsuario,
                                [nome, username, email, hash, nascimento, sexo, result.insertId],
                                (err, results, fields) => {
                                    if (err) {
                                        conn.release();
                                        res.status(500).json({
                                            error: err,
                                            message: "Erro ao conectar banco."
                                        });
                                    } else {
                                        conn.release();
                                        res.status(201).json({
                                            error: null,
                                            message: 'Usuário registrado com sucesso!'
                                        })
                                    }
                                })
                        });
                    }
                });
            }
        })
    }
}

exports.editarUsuario = (req, res, next) => {

    // const {
    //     idUsuario,
    //     nome,
    //     username,
    //     email,
    //     senha,
    //     nascimento,
    //     sexo,
    //     rua,
    //     numero,
    //     complemento,
    //     bairro,
    //     idCidade
    // } = req.body;

    if (!req.body.senha_atual) {
        res.status(500).json({
            error: null,
            message: 'Erro de conexão com banco de dados.'
        });
    };

    var id_usuario = req.params.id_usuario;
    var nome = '';
    var username = '';
    var sexo = '';
    var senha = '';

    if (req.body.nome) {
        nome = req.body.nome
    };
    if (req.body.username) {
        username = req.body.username
    };
    if (req.body.sexo) {
        sexo = req.body.sexo
    };
    if (req.body.senha) {
        senha = req.body.senha
    };

    dbPool.getConnection((error, conn) => {
        conn.query(`SELECT *
                    FROM
                    Usuarios
                    WHERE id_usuario = '${req.params.id_usuario}'`, (error, results, fields) => {
            if (error) {
                res.status(406).json({
                    error: error,
                    message: 'É necessário a senha para editar o usuário.'
                });
            }
            if (results) {
                bcrypt.compare(req.body.senha_atual, results.senha, (err, result) => {
                    if (err) {
                        conn.release();
                        res.status(401).json({
                            error: err,
                            message: 'Falha na autenticação.'
                        })
                    }
                    if (result) {

                        bcrypt.hash(senha, 10, (err, hash) => {
                            if (err) {
                                conn.release();
                                res.status(500).json({
                                    error: err,
                                    message: 'Erro de encriptação.'
                                });
                            }
                        });

                        const novosDados = `UPDATE Usuarios
                                                        SET nome        = '${nome}'
                                                            username    = '${username}'
                                                            senha       = '${hash}'
                                                            sexo        = '${sexo}'
                                                            WHERE id_usuario = '${id_usuario}'
                                                            `;
                        conn.query(novosDados, (error1, results1, fields1) => {
                            conn.release();
                            if (error1) {
                                res.status(500).json({
                                    error: error1,
                                    message: 'Não foi possível atualizar os dados.'
                                });
                            }
                            if (results1) {
                                res.status(200).json({
                                    error: null,
                                    message: 'Atualizado com sucesso!'
                                })
                            }
                        })
                    }
                })
            }
        })
    })
}


exports.deletarUsuario = (req, res, next) => {
    const usuarioDeletado = `DELETE
                             FROM
                             Usuarios
                             WHERE id_usuario = '${req.body.id_usuario}'`;

    dbPool.getConnection((error, conn) => {
        if (error) {
            conn.release();
            res.status(500).json({
                error: error,
                message: 'Erro ao conectar banco.'
            })
        } else {
            conn.release();
            conn.query(usuarioDeletado, (error, results, fields) => {
                if (error) {
                    throw error;
                } else {
                    res.status(200).json({
                        error: null,
                        message: 'Deletado com sucesso.'
                    })
                }
            })
        }
    })
}

exports.login = (req, res, next) => {

    const {
        emailOrUsername,
        senha
    } = req.body;

    console.log(req.body);

    if (!emailOrUsername || !senha) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {
        const queryLogin = `select * from Usuarios where username = ? or email = ?;`;

        dbPool.getConnection((error, conn) => {
            conn.query(queryLogin, [emailOrUsername, emailOrUsername], (error, results, fields) => {
                conn.release();
                if (error) {
                    res.status(500).json({
                        error: error,
                        message: 'Erro de conexão com banco.'
                    })
                } else {
                    if (results.length < 1) {
                        res.status(401).json({
                            error: null,
                            message: 'Não existe esse usuario.'
                        })
                    } else {
                        bcrypt.compare(senha, results[0].senha, (err, result) => {
                            if (err) {
                                res.status(401).json({
                                    error: err,
                                    message: 'Falha na autenticação.'
                                })
                            }
                            if (result) {
                                const token = jwt.sign({
                                    id_usuario: results[0].id_usuario,
                                    username: results[0].username
                                }, "2fiit");

                                res.status(200).json({
                                    error: null,
                                    message: 'Logado com sucesso.',
                                    token: token,
                                    dados: {
                                        id_usuario: results[0].id_usuario,
                                        username: results[0].username,
                                        nome: results[0].nome,
                                        objetivo: "",
                                        peso: "",
                                        altura: "",
                                        nivelSedentarismo: "",
                                        descricao: ""
                                    }
                                })
                            } else {
                                res.status(401).json({
                                    error: null,
                                    message: 'Senha incorreta!'
                                })
                            }
                        })
                    }
                }
            })
        })
    }
}

exports.solicitarTreinador = (req, res, next) => {

    const { token, idTreinador } = req.body;

    if (!token || !idTreinador) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {

        dbPool.getConnection((err, conn) => {
            if (err) {
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {

                let tokenDecoded = jwt.verify(token, "2fiit");

                let insertSolicitaTreino = `
                    insert into Usuarios_has_Treinador
                    (
                        Usuarios_id_usuario,
                        Treinador_idTreinador
                    )
                    values
                        ( ?, ? );               
                `;
                
                conn.query(insertSolicitaTreino,
                    [tokenDecoded["id_usuario"], idTreinador],
                    (err, result, fields) => {
                    
                    if (err) {
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        res.status(200).json({
                            "error": null,
                            "response": "Solicitação criada com Sucesso!"
                        });
                    }
                });

            }
        });

    }

}