const dbPool = require('../config/database');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

exports.login = function (req, res) {

    let {
        email,
        senha
    } = req.body;

    if (!email || !senha) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.release();
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {

                let search = `
                    select
                        tr.idTreinador,
                        tr.nome,
                        tr.email,
                        tr.senha,
                        tr.cref,
                        tr.especialidade,
                        tr.formacao,
                        tr.descricao,
                        tr.dataNascimento,
                        tr.dataCadastro,
                        tr.sexo,
                        en.rua,
                        en.numero,
                        en.complemento,
                        en.cep,
                        en.bairro,
                        ci.nome as cidade,
                        es.nome as estado 
                    from Treinador tr
                        left join Endereco en
                            on tr.idEndereco = en.idEndereco
                        left join Cidades ci
                            on en.idCidade = ci.idCidade
                        left join Estados es
                            on ci.idEstado = es.idEstado
                    where tr.email = ?
                `;

                conn.query(search, email, function (err, results, fields) {
                    if (err) {
                        conn.release();
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        if (results.length < 1) {
                            conn.release()
                            res.status(200).json({
                                "error": null,
                                "response": "Treinador não encontrado!"
                            });
                        } else {
                            bcrypt.compare(senha, results[0].senha, function (err, auth) {
                                if (err) {
                                    conn.release();
                                    res.status(406).json({
                                        'response': null,
                                        'error': err
                                    });
                                } else {
                                    if (!auth) {
                                        conn.release();
                                        res.status(406).json({
                                            'error': null,
                                            'response': {
                                                'auth': false
                                            }
                                        });
                                    } else {
                                        const token = jwt.sign({
                                            id_treinador: results[0].idTreinador,
                                            email: results[0].email
                                        }, "2fiit");

                                        conn.release();

                                        res.status(202).json({
                                            'error': null,
                                            'token': token,
                                            'response': {
                                                'auth': true,
                                                'data': {
                                                    nome: results[0].nome,
                                                    especialidade: results[0].especialidade,
                                                    cidade: results[0].cidade,
                                                    descricao: results[0].descricao,
                                                    cref: results[0].cref,
                                                    formacao: results[0].formacao,
                                                    imagem: results[0].imagem
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }
}

exports.listarTreinadores = function (req, res) {
    dbPool.getConnection((err, conn) => {
        if (err) {
            conn.release();
            res.status(500).json({
                "response": null,
                "error": err
            });
        } else {
            let search = `
                select
                    tr.idTreinador,
                    tr.nome,
                    tr.email,
                    tr.cref,
                    tr.especialidade,
                    tr.formacao,
                    tr.descricao,
                    tr.dataNascimento,
                    tr.dataCadastro,
                    tr.sexo,
                    en.rua,
                    en.numero,
                    en.complemento,
                    en.cep,
                    en.bairro,
                    ci.nome as cidade,
                    es.nome as estado 
                from Treinador tr
                    left join Endereco en
                        on tr.idEndereco = en.idEndereco
                    left join Cidades ci
                        on en.idCidade = ci.idCidade
                    left join Estados es
                        on ci.idEstado = es.idEstado;
            `;
            conn.query(search, function (err, results, fields) {
                if (err) {
                    conn.release();
                    res.status(500).json({
                        "response": null,
                        "error": err
                    });
                } else {
                    res.status(200).json({
                        error: false,
                        treinadores: results,
                        message: 'lista de treinador.'
                    });
                }
            });
        }
    });
};

exports.findById = function (req, res) {

    let id_treinador = req.params.id;

    if (!id_treinador) {
        return res.status(400).json({
            error: true,
            message: 'Insira id treinador pela rota.'
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.release();
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {

                let search = `
                    select
                        tr.idTreinador,
                        tr.nome,
                        tr.email,
                        tr.cref,
                        tr.especialidade,
                        tr.formacao,
                        tr.descricao,
                        tr.dataNascimento,
                        tr.dataCadastro,
                        tr.sexo,
                        en.rua,
                        en.numero,
                        en.complemento,
                        en.cep,
                        en.bairro,
                        ci.nome as cidade,
                        es.nome as estado 
                    from Treinador tr
                        left join Endereco en
                            on tr.idEndereco = en.idEndereco
                        left join Cidades ci
                            on en.idCidade = ci.idCidade
                        left join Estados es
                            on ci.idEstado = es.idEstado
                    where idTreinador = ?;
                `;

                conn.query(search, id_treinador, function (err, results, fields) {
                    if (err) {
                        conn.release();
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        conn.release();
                        res.status(200).json({
                            error: false,
                            treinador: results[0],
                            message: 'Treinador.'
                        });
                    }
                });
            }
        });
    }
};

exports.insert = function (req, res) {
    const {
        nome,
        senha,
        email
    } = req.body;

    if (!nome || !senha || !email) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {

        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.release();
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {
                const novoTreinador = `
                    insert into Treinador
                    (nome,
                    email,
                    senha)
                    values
                    ( ?, ?, ? );
                `;

                bcrypt.hash(senha, 10, (err, hash) => {
                    conn.query(novoTreinador,
                        [nome, email, hash],
                        (err, results, fields) => {
                            if (err) {
                                conn.release();
                                res.status(500).json({
                                    error: err,
                                    message: "Erro ao conectar banco."
                                });
                            } else {
                                conn.release();
                                res.status(201).json({
                                    error: null,
                                    message: 'Treinador registrado com sucesso!'
                                })
                            }
                        })
                });
            }
        });

    }
}

exports.update = function (req, res) {

    let {
        nome,
        cref,
        formacao,
        especialidade,
        descricao,
        dataNascimento,
        DadosBancarios,
        sexo,
        rua,
        numero,
        complemento,
        imagem,
        cep,
        bairro,
        idCidade,
        token
    } = req.body;

    if (!nome || !cref || !formacao || !descricao || !sexo ||
        !dataNascimento || !rua || !numero || !complemento || !especialidade ||
        !bairro || !idCidade || !token || !cep) {
        return res.status(400).send({
            error: true,
            message: 'Invalid Params!'
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.release();
                res.status(500).json({
                    error: err,
                    message: 'Erro ao cadastrar.'
                });
            } else {

                if (!imagem) {
                    imagem = "";
                }

                let tokenDecoded = jwt.verify(token, '2fiit');

                const buscarTreinador = `
                    select * from Treinador where idTreinador = ?
                `;

                conn.query(buscarTreinador, [tokenDecoded['id_treinador']], (err, result, fields) => {
                    if (err) {
                        conn.release();
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        if (result.length < 1) {
                            conn.release();
                            res.status(500).json({
                                "response": null,
                                "error": "Not Found"
                            });
                        } else {
                            console.log(result[0].idEndereco);
                            if (result[0].idEndereco == null) {
                                const novoEndereco = `
                                insert into Endereco
                                (rua,
                                numero,
                                complemento,
                                bairro,
                                idCidade)
                                values
                                ( ?, ?, ?, ?, ? );
                                `;

                                conn.query(novoEndereco, [rua, numero, complemento, bairro, idCidade], (err, result) => {
                                    if (err) {
                                        conn.release();
                                        res.status(500).json({
                                            "response": null,
                                            "error": err
                                        });
                                    } else {
                                        const novoTreinador = `
                                            UPDATE Treinador
                                            SET
                                                nome = ?,
                                                cref = ?,
                                                formacao = ?,
                                                especialidade = ?,
                                                imagem = ?,
                                                descricao = ?,
                                                dataNascimento = ?,
                                                idEndereco = ?,
                                                sexo = ?
                                            WHERE 
                                                idTreinador = ?
                                        `;

                                        conn.query(novoTreinador,
                                            [nome, cref, formacao, especialidade, imagem, descricao, dataNascimento, result.insertId, sexo, tokenDecoded['id_treinador']],
                                            (err, results, fields) => {
                                            if (err) {
                                                conn.release();
                                                res.status(500).json({
                                                    error: err,
                                                    message: "Erro ao conectar banco."
                                                });
                                            } else {
                                                conn.release();
                                                res.status(201).json({
                                                    error: null,
                                                    message: 'Treinador atualizado com sucesso!'
                                                })
                                            }
                                        })
                                    }
                                });
                            } else {

                                const novoEndereco = `
                                    UPDATE Endereco
                                    SET
                                        rua = ?,
                                        numero = ?,
                                        complemento = ?,
                                        cep = ?,
                                        bairro = ?,
                                        idCidade = ?
                                    WHERE
                                        idEndereco = ?;
                                `;

                                conn.query(novoEndereco, [rua, numero, complemento, cep, bairro, idCidade, result[0].idEndereco], (err, results) => {
                                    if (err) {
                                        conn.release();
                                        res.status(500).json({
                                            "response": null,
                                            "error": err
                                        });
                                    } else {
                                        const novoTreinador = `
                                            UPDATE Treinador
                                            SET
                                                nome = ?,
                                                cref = ?,
                                                formacao = ?,
                                                especialidade = ?,
                                                imagem = ?,
                                                descricao = ?,
                                                dataNascimento = ?,
                                                idEndereco = ?,
                                                sexo = ?
                                            WHERE 
                                                idTreinador = ?
                                        `;

                                        conn.query(novoTreinador,
                                            [nome, cref, formacao, especialidade, imagem, descricao, dataNascimento, result[0].idEndereco, sexo, tokenDecoded['id_treinador']],
                                            (err, results, fields) => {
                                            if (err) {
                                                conn.release();
                                                res.status(500).json({
                                                    error: err,
                                                    message: "Erro ao conectar banco."
                                                });
                                            } else {
                                                conn.release();
                                                res.status(201).json({
                                                    error: null,
                                                    message: 'Treinador atualizado com sucesso!'
                                                })
                                            }
                                        })
                                    }
                                });
                            }
                        }
                    }
                });
            }
        })
    }
};

exports.editar = function (req, res) {

    let id_treinador = req.params.id;
    let nome = req.body.Nome;
    let cref = req.body.Cref;
    let formacao = req.body.Formacao;
    let descricao = req.body.Descricao;
    let dataNascimento = req.body.DataNascimento;
    let dadosBancarios = req.body.DadosBancarios;
    let senha = bcrypt.hashSync(req.body.Senha, 10);
    let id_redeSocial = req.body.Id_redeSocial;
    let id_endereco = req.body.Id_endereco;

    if (!nome) {
        res.status(400).json({
            error: nome,
            message: 'Insira o nome treinador e /id pela rota.'
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.release();
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {
                conn.query("UPDATE Treinador SET ? WHERE Id_treinador= ?", [{
                    Nome: nome,
                    Cref: cref,
                    Formacao: formacao,
                    Descricao: descricao,
                    DataNascimento: dataNascimento,
                    DadosBancarios: dadosBancarios,
                    Senha: senha,
                    Id_redeSocial: id_redeSocial,
                    Id_endereco: id_endereco
                }, id_treinador], function (err, results, fields) {
                    if (err) {
                        conn.release();
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        conn.release();
                        res.status(200).json({
                            error: false,
                            data: results,
                            message: 'Treinador atualizado com sucesso!'
                        });
                    }
                });
            }
        });
    }
};

exports.deletar = function (req, res) {

    let id_treinador = req.body.Id_treinador;

    if (!id_treinador) {
        return res.status(400).send({
            error: true,
            message: 'insira Id_treinador'
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                conn.release();
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {
                dbConn.query('DELETE FROM Treinador WHERE Id_treinador = ?', [id_treinador], function (err, results, fields) {
                    if (err) {
                        conn.release();
                        res.status(500).json({
                            "response": null,
                            "error": err
                        });
                    } else {
                        conn.release();
                        res.status(200).json({
                            error: false,
                            data: results,
                            message: 'treinador deletado com sucesso!'
                        });
                    }
                });
            }
        });
    }
};

exports.aceitarUsuario = (req, res, next) => {

    const {
        token,
        idUsuario
    } = req.body;

    if (!token || !idUsuario) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {

                let tokenDecoded = jwt.verify(token, "2fiit");

                let updateSolicitaTreino = `
                    UPDATE Usuarios_has_Treinador
                    SET
                        eTreinador = 1
                    WHERE
                        Usuarios_id_usuario = ? AND
                        Treinador_idTreinador = ? AND
                        eTreinador = 0;
                `;

                conn.query(updateSolicitaTreino,
                    [idUsuario, tokenDecoded["id_treinador"]],
                    (err, result, fields) => {

                        if (err) {
                            res.status(500).json({
                                "response": null,
                                "error": err
                            });
                        } else {
                            res.status(200).json({
                                "error": null,
                                "response": "Solicitação aceita com Sucesso!"
                            });
                        }
                    });

            }
        });

    }

}

exports.rejeitarUsuario = (req, res, next) => {

    const {
        token,
        idUsuario
    } = req.body;

    if (!token || !idUsuario) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {

                let tokenDecoded = jwt.verify(token, "2fiit");

                let deleteSolicitaTreino = `
                    DELETE FROM 
                            Usuarios_has_Treinador
                    WHERE 
                            Usuarios_id_usuario = ? AND
                            Treinador_idTreinador = ? AND
                            eTreinador = 0;
                `;

                conn.query(deleteSolicitaTreino,
                    [idUsuario, tokenDecoded["id_treinador"]],
                    (err, result, fields) => {

                        if (err) {
                            res.status(500).json({
                                "response": null,
                                "error": err
                            });
                        } else {
                            res.status(200).json({
                                "error": null,
                                "response": "Solicitação rejeitada com Sucesso!"
                            });
                        }
                    });

            }
        });

    }

}

exports.listarUsuarios = (req, res, next) => {

    const {
        token,
        status
    } = req.body;

    if (!token || status == undefined) {
        res.status(406).json({
            "response": null,
            "error": "Invalid Params!"
        });
    } else {
        dbPool.getConnection((err, conn) => {
            if (err) {
                res.status(500).json({
                    "response": null,
                    "error": err
                });
            } else {

                let tokenDecoded = jwt.verify(token, "2fiit");

                let findUsers = `
                    SELECT 
                        * 
                    FROM 
                        Usuarios_has_Treinador 
                    WHERE 
                        Treinador_idTreinador = ? AND
                        eTreinador = ?;
                `;

                conn.query(findUsers,
                    [tokenDecoded["id_treinador"], status],
                    (err, results, fields) => {

                        if (err) {
                            res.status(406).json({
                                "response": null,
                                "error": err
                            });
                        } else {

                            res.status(200).json({
                                "response": results,
                                "error": null
                            });

                        }

                    });
            }
        });
    }

}