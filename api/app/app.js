const express = require('express');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/usersR');
const treinadorRoutes = require('./routes/treinadorR');
const redeSocialRoutes = require('./routes/redeSocialR');
const cidadeRoutes = require('./routes/cidadeR');

const app = express();

app.use(bodyParser.json());
app.use(express.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use('/user', userRoutes);
app.use('/treinador', treinadorRoutes);
app.use('/rede-social', redeSocialRoutes);
app.use('/cidade', cidadeRoutes);

module.exports = app;