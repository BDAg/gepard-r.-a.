const jwt = require('jsonwebtoken');

exports.required = (req, res, next) => {
    res.locals.id_usuario = 0;

    try {

        const token = req.headers.authorization.split("")[1];
        const decoded = jwt.verify(token, "2fiit");
        req.userData = decoded;
        res.locals.id_usuario = req.userData.id_usuario;

        next();
    } catch(error) {
        return res.send(401).json({
            error: null,
            message: 'Usuario não autenticado.'
        });
    };
};