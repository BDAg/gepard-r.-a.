const express = require('express');
const router = express.Router();
const rotaCidade = require('../controllers/cidadeC');

router.get('/list', rotaCidade.find);

module.exports = router;