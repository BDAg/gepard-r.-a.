const express = require('express');

const router = express.Router();

const rotaTreinador = require('../controllers/treinadorC');

router.get('/treinador', rotaTreinador.listarTreinadores);

router.get('/treinador/:id', rotaTreinador.findById);

router.post('/treinador', rotaTreinador.insert);

router.post('/update', rotaTreinador.update);

router.post('/login', rotaTreinador.login);

router.put('/treinador/:id', rotaTreinador.editar);

router.delete('/treinador', rotaTreinador.deletar);

router.post('/aceitar-usuario', rotaTreinador.aceitarUsuario);

router.post('/rejeitar-usuario', rotaTreinador.rejeitarUsuario);

router.post('/listar-usuario', rotaTreinador.listarUsuarios);

module.exports = router;