const express = require('express');

const router = express.Router();

const redeSocialRoutes = require('../controllers/redeSocialC');

router.delete('/redeSocial', redeSocialRoutes.deletar);
router.post('/redeSocial', redeSocialRoutes.inserir);
router.put('/redeSocial/:id', redeSocialRoutes.editar);

module.exports = router;