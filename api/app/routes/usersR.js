const express = require('express');
const controller = require('../controllers/userC');

const router = express.Router();
const login = require('../middleware/login')

router.get('/usuarios', controller.buscarUsuarios);

router.post('/usuario', controller.insert);

router.post('/update', controller.update);

router.post('/login', controller.login);

router.put('/editarperfil', login.required, controller.editarUsuario);

router.delete('/deletar', controller.deletarUsuario);

router.post('/solicitar-treinador', controller.solicitarTreinador);

module.exports = router;