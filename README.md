# Gepard Running Assistent

## Documentação 

### Sobre o projeto

O app de monitoramento de corrida, contendo um ambiente de gerenciamento para o usuário e treinador envolvendo corrida indoor (esteira) e outdoor (em qualquer lugar, rua). 

[Saber mais.](https://gitlab.com/BDAg/gepard-r.-a./wikis/home)

[Cronograma.](https://gitlab.com/BDAg/gepard-r.-a./wikis/Cronograma)

[Mapa de definição tecnológica.](https://gitlab.com/BDAg/gepard-r.-a./wikis/Mapa-de-defini%C3%A7%C3%A3o-tecnol%C3%B3gica)

[Tutorial de instalação.](https://gitlab.com/BDAg/gepard-r.-a./wikis/Documenta%C3%A7%C3%A3o-da-Solu%C3%A7%C3%A3o)

### Andamento do Projeto

- [x] [Definições de Projeto](https://gitlab.com/BDAg/gepard-r.-a./wikis/home)
- [x] [Documentação da Solução](https://gitlab.com/BDAg/gepard-r.-a./wikis/Documenta%C3%A7%C3%A3o-da-Solu%C3%A7%C3%A3o)
- [x] Desenvolvimento da Solução (Sprint 1)
- [x] Desenvolvimento da Solução (Sprint 2)
- [x] Desenvolvimento da Solução (Sprint 3)
- [x] Fechamento do Projeto

### Equipe

- [Dério Lucas (CTO)](https://gitlab.com/BDAg/gepard-r.-a./wikis/D%C3%A9rio-Lucas)

- [Letícia Melo (CTO)](https://gitlab.com/BDAg/gepard-r.-a./wikis/Let%C3%ADcia-Melo)

- [José Eduardo Maran](https://gitlab.com/BDAg/gepard-r.-a./wikis/Maran)

- [Pedro Zulian](https://gitlab.com/BDAg/gepard-r.-a./wikis/Pedro-Zulian)

- [Bruno Anthony Shimura](https://gitlab.com/BDAg/gepard-r.-a./wikis/Bruno)
