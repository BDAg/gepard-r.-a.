/* eslint-disable no-unused-vars */
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Login from './pages/login';
import CadastroPersonal from './pages/cadastroPersonal';
import RecuperarSenha from './pages/redefinirSenha';
import CadastroInicial from './pages/cadastroInicial';
import HomePersonal from './pages/homePersonal';
import HomeAluno from './pages/homeAluno';
import PerfilAluno from './pages/perfilAluno';
import PerfilPersonal from './pages/perfilPersonal';
import ProcurarTreinador from './pages/procurarTreinador';
import CustomDrawer from './components/CustomDrawer';
import CadastroUsuario from './pages/cadastroAluno';

const Routes = createStackNavigator(
  {
    TelaLogin: Login,
    TelaCadastroPersonal: CadastroPersonal,
    TelaRecuperarSenha: RecuperarSenha,
    TelaCadastroInicial: CadastroInicial,
    TelaHomePersonal: HomePersonal,
    TelaHomeAluno: HomeAluno,
    TelaPerfilAluno: PerfilAluno,
    TelaPerfilPersonal: PerfilPersonal,
    TelaProcurarTreinador: ProcurarTreinador,
    TelaCadastroUsuario: CadastroUsuario,
  },
  {
    initialRouteName: 'TelaLogin',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#6b6e70',
      },
      headerTintColor: '#FFF',
    },
    headerMode: 'screen',
  },
);

export default createAppContainer(Routes);
