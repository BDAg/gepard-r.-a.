import axios from 'axios';

const url = 'http://35.231.220.66:4230';

export const apiServices = {
  loginUser,
  loginTreinador,
  cadastroInicialUser,
  cadastroInicialTreinador,
  cadastroUser,
  cadastroTreinador,
  solicitarTreinador,
  listarUsuario,
  aceitarUsuario,
  rejeitarUsuario
};

function loginUser(emailOrUsername, senha, callback = null) {
  axios({
    url: `${url}/user/login`,
    method: "POST",
    data: {
      "emailOrUsername": emailOrUsername,
      "senha": senha
    }
  })
    .then(response => {
      callback(null, response);
    })
    .catch(err => callback(err, null));
}


function loginTreinador(email, senha, callback = null) {
  axios({
    url: `${url}/teinador/login`,
    method: "POST",
    data: {
      "email": email,
      "senha": senha
    }
  })
    .then(response => {
      callback(null, response);
    })
    .catch(err => callback(err, null));
}

function cadastroInicialUser(nome, email, senha, callback = null) {
  axios({
    url: `${url}/user/usuario`,
    method: "POST",
    data: {
      "nome": nome,
      "email": email,
      "senha": senha
    }
  })
  .then(response => {
    callback(null, response);
  })
  .catch(err => callback(err, null));
}

function cadastroInicialTreinador(nome, email, senha, callback = null) {
  axios({
    url: `${url}/treinador/treinador`,
    method: "POST",
    data: {
      "nome": nome,
      "senha": senha,
      "email": email
    }
  })
  .then(response => {
    callback(null, response);
  })
  .catch(err => callback(err, null));
}

function cadastroUser(nome, username, email, senha, dataNascimento, sexo, rua, numero, complemento, bairro, cep, idCidade, altura, peso, imagem, token, callback = null) {
  axios({
    url: `${url}/user/update`,
    method: "POST",
    data: {
      "nome": nome,
      "username": username,
      "email": email,
      "senha": senha,
      "dataNascimento": dataNascimento,
      "sexo": sexo,
      "rua": rua,
      "numero": numero,
      "complemento": complemento,
      "bairro": bairro,
      "cep": cep,
      "idCidade": idCidade,
      "altura": altura,
      "peso": peso,
      "imagem": imagem,
      "token": token
    }
  })
  .then(response => {
    callback(null, response);
  })
  .catch(err => callback(err, null));
}

function cadastroTreinador(nome, cref, formacao, descricao, dataNascimento, DadosBancarios, sexo, senha, email, rua, numero, cep, especialidade, imagem, complemento, bairro, idCidade, token, callback = null) {
  axios({
    url: `${url}/treinador/update`,
    method: "POST",
    data: {
      "nome": nome,
      "cref": cref,
      "formacao": formacao,
      "descricao": descricao,
      "dataNascimento": dataNascimento,
      "DadosBancarios": DadosBancarios,
      "sexo": sexo,
      "senha": senha,
      "email": email,
      "rua": rua,
      "numero": numero,
      "cep": cep,
      "especialidade": especialidade,
      "imagem": imagem,
      "complemento": complemento,
      "bairro": bairro,
      "idCidade": idCidade,
      "token": token
    }
  })
  .then(response => {
    callback(null, response);
  })
  .catch(err => callback(err, null));
}

function solicitarTreinador(token, idTreinador, callback = null) {
  axios({
    url: `${url}/user/solicitar-treinador`,
    method: "POST",
    data: {
      "token": token,
      "idTreinador": idTreinador
    }
  })
    .then(response => {
      callback(null, response);
    })
    .catch(err => callback(err, null));
}

function listarUsuario(token, status, callback = null) {
  axios({
    url: `${url}/treinador/listar-usuario`,
    method: "POST",
    data: {
      "token": token,
      "status": status
    }
  })
    .then(response => {
      callback(null, response);
    })
    .catch(err => callback(err, null));
}

function aceitarUsuario(token, idUsuario, callback = null) {
  axios({
    url: `${url}/treinador/aceitar-usuario`,
    method: "POST",
    data: {
      "token": token,
      "idUsuario": idUsuario
    }
  })
    .then(response => {
      callback(null, response);
    })
    .catch(err => callback(err, null));
}

function rejeitarUsuario(token, idUsuario, callback = null) {
  axios({
    url: `${url}/treinador/rejeitar-usuario`,
    method: "POST",
    data: {
      "token": token,
      "idUsuario": idUsuario
    }
  })
    .then(response => {
      callback(null, response);
    })
    .catch(err => callback(err, null));
}

// async function getInstrucoes(callback = null) {
//   axios({ url: `${url}/instrucoes/list`, method: 'GET', data: '' })
//     .then(response => {
//       if (response.status !== 200) {
//         callback(Error('Failed List'), []);
//       } else {
//         callback(null, response);
//       }
//     })
//     .catch(err => {
//       callback(err, []);
//     });
// }