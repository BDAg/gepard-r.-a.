import React, { Component } from 'react';
import { TextInput, TouchableOpacity, View, Image } from 'react-native';

const styles = require('./styles');

export default class SearchBar extends Component {
  render() {
    return (
      <View style={styles.searchBarContainer}>
        <TextInput placeholder='Pesquisar' style={styles.textInputSearch} underlineColorAndroid={'transparent'} />
        <Image
          style={styles.Lupa}
          source={require('../../assets/lupa.png')}
        />
      </View>
    )
  }
}