import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  Foto: {
    width: 50,
    height: 50,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: 50,
    top: -10,
  },
  NomeView: {
    top: -10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    color: '#FFF',
    fontSize: 20,
  },
});

export default styles;
