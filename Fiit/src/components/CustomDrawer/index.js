import React from 'react';
import { View, Text, ScrollView, Image } from 'react-native';
import { Header, Input } from 'react-native-elements';
import styles from './styles';
import { DrawerNavigatorItems } from 'react-navigation-drawer';

export default function CustomDrawer({ ...props }) {
  return (
    <View>
      <ScrollView>
        <Header
          style={styles.Header}
          containerStyle={{ backgroundColor: '#61892f' }}>
          <Image
            style={styles.Foto}
            source={require('../../assets/foto.png')}
          />
          <Text style={styles.NomeView}>Gabriel do Carmo</Text>
          <Image style={styles.Menu} source={require('../../assets/menu.png')} />
        </Header>
        <DrawerNavigatorItems {...props} />
      </ScrollView>
    </View>
  );
}
