import {StatusBar} from 'react-native';

StatusBar.setBackgroundColor('#222629');
StatusBar.setBarStyle('light-content');
