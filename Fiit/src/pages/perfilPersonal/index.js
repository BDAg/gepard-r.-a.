import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  Icon,
} from 'react-native';
import styles from './styles';
import {Header} from 'react-native-elements';
// import { Container } from './styles';

export default class PerfilPersonal extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: () => (
        <Image style={styles.Logo} source={require('../../assets/logo.png')} />
      ),
      headerRight: () => (
        <Image style={styles.Menu} source={require('../../assets/menu.png')} />
      ),
    };
  };
  render() {
    return (
      <View>
        <Header
          style={styles.Header}
          containerStyle={{
            backgroundColor: 'white',
            borderBottomColor: '#6b6e70',
            borderBottomWidth: 2,
          }}>
          <Image
            style={styles.Foto}
            source={require('../../assets/aluno4.jpg')}
          />
          <Text style={styles.NomeView}>Gabriel do Carmo</Text>
        </Header>
        <ScrollView>
          <Image
            style={styles.FotoPersonal}
            source={require('../../assets/personal1.jpg')}
          />
          <Text style={styles.Nome}>Nome: </Text>
          <Text style={styles.Especialidade}>Especialidade:</Text>
          <Text style={styles.Cidade}>Cidade:</Text>
          <Text style={styles.Descricao}>Descrição:</Text>
          <Text style={styles.Cref}>CREF:</Text>
          <Text style={styles.Formacao}>Formação:</Text>
          <Text style={styles.Contato}>Contato:</Text>
          <Text style={styles.RedesSociais}>RedesSociais:</Text>
        </ScrollView>
      </View>
    );
  }
}
