import React, { Component } from 'react';
import { Text, TextInput, View, Alert, TouchableHighlight, Image, ScrollView, TouchableOpacity } from 'react-native';
import { Tab, Tabs, Container, Footer, FooterTab, Button, Icon, Content, ListItem, List, Left, Right, Body, Thumbnail } from 'native-base';
import { Header, SocialIcon } from 'react-native-elements';
import { Searchbar } from 'react-native-paper';

import styles from './styles';

export default class HomePersonal extends Component {
  state = {
    firstQuery: '',
  };
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => (
        <Image style={styles.Logo} source={require('../../assets/logo.png')} />
      ),
      headerRight: () => (
        <Image style={styles.Menu} source={require('../../assets/menu.png')} />
      ),
    };
  };
  render() {
    const { firstQuery } = this.state;
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Header
          containerStyle={{
            backgroundColor: 'transparent',
            borderRadius: 4,
            borderWidth: 0.5,
            borderColor: '#d6d7da',
          }}>
          <TouchableOpacity
            style={styles.botaoAvatar}
            onPress={() => navigate('TelaCadastroPersonal')}>
            <Image
              style={styles.Foto}
              source={require('../../assets/foto.png')}
            />
          </TouchableOpacity>
          <Text style={styles.NomeView}>Gabriel do Carmo</Text>
        </Header>
        <View>
          <Searchbar
            placeholder="Pesquisar"
            onChangeText={query => { this.setState({ firstQuery: query }); }}
            value={firstQuery}
          />
        </View>
        <Footer>
          <Container style={[styles.margin]}>
            <Tabs tabBarUnderlineStyle={{ backgroundColor: '#6b6e70' }} initialPage={0}>
              <Tab tabStyle={{ backgroundColor: 'white' }}
                activeTabStyle={{ backgroundColor: 'white' }}
                textStyle={{ color: 'black' }}
                activeTextStyle={{ color: 'black' }} heading="Alunos">
                <View style={[styles.container, { backgroundColor: 'white' }]}>
                  <Text></Text>
                </View>
                <ScrollView>
                  <Content>
                    <List>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'https://cdn.pensador.com/img/imagens/pe/xe/pexels_photo_4_2_c.jpg' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 1</Text>
                        </Body>
                      </ListItem>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'http://blog.seguridade.com.br/wp-content/uploads/2018/10/211396-saiba-como-o-perfil-do-profissional-de-seguranca-deve-ser-810x541.jpg' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 2</Text>
                        </Body>
                      </ListItem>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'https://www.santopapo.com.br/wp-content/uploads/Como-tirar-uma-boa-foto-para-o-seu-perfil.jpg' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 3</Text>
                        </Body>
                      </ListItem>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'https://abrilexame.files.wordpress.com/2018/10/8dicas6.jpg?quality=70&strip=info' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 4</Text>
                        </Body>
                      </ListItem>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'https://mariascarlet.files.wordpress.com/2017/03/mulher-de-preto-5.jpeg?w=600' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 5</Text>
                        </Body>
                      </ListItem>
                    </List>
                  </Content>
                </ScrollView>
              </Tab>
              <Tab tabStyle={{ backgroundColor: 'white' }}
                activeTabStyle={{ backgroundColor: 'white' }}
                textStyle={{ color: 'black' }}
                activeTextStyle={{ color: 'black' }} heading="Solicitações">
                <View style={[styles.container, { backgroundColor: 'white' }]}>
                  <Text></Text>
                </View>
                <ScrollView>
                  <Content>
                    <List>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'https://assets-br.wemystic.com.br/20191113010255/homem-peixes-850x640.jpg' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 1</Text>
                        </Body>
                        <Right>
                          <Button>
                            <Icon name='checkmark' />
                          </Button>
                        </Right>
                        <Right>
                          <Button>
                            <Icon name='close' />
                          </Button>
                        </Right>
                      </ListItem>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'https://www.bslidiomas.com.br/wp-content/uploads/2019/02/Como-melhorar-o-curriculo-profissional-com-o-curso-de-ingles-4.jpg' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 2</Text>
                        </Body>
                      </ListItem>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'https://www.portrasdafoto.com/wp-content/uploads/2017/10/perfil-teste.jpg' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 3</Text>
                        </Body>
                      </ListItem>
                      <ListItem avatar>
                        <Left>
                          <Thumbnail source={{ uri: 'https://viverdemaquininha.com.br/wp-content/uploads/2019/08/perfil-do-franqueado-capa.jpg' }} />
                        </Left>
                        <Body>
                          <Text>Aluno 5</Text>
                        </Body>
                      </ListItem>
                    </List>
                  </Content>
                </ScrollView>
              </Tab>
            </Tabs>
          </Container>
        </Footer>
      </View>
    );
  }
  clicou = () => {
    Alert.alert('Login', 'Logando');
  };
  redefinirSenha = () => {
    Alert.alert('Esqueci senha', 'Redefinir Senha');
  };
  cadastrarUsuario = () => {
    Alert.alert('Registrar', 'Registrado');
  };
}
