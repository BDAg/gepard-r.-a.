import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  Logo: {
    width: 125,
    height: 50,
    margin: 10,
  },
  Menu: {
    width: 50,
    height: 50,
    margin: 10,
  },
  Foto: {
    width: 50,
    height: 50,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50,
    top: -10,
  },
  NomeView: {
    top: -10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    marginRight: 50,
  },
  // Search: {
  //   flex: 1,
  //   backgroundColor: '#fff',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // },
});
export default styles;