/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert,
  Icon,
} from 'react-native';
import {Left} from 'native-base';
import {Header, Input} from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox';
import ImagePicker from 'react-native-image-picker';
import styles from './styles';
import {RadioButton} from 'react-native-paper';

export default class CadastroPersonal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '2019-11-13',
      data: '2019-10-20',
      filePath: {},
    };
  }

  chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        {name: 'customOptionKey', title: 'Choose Photo from Custom Option'},
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          filePath: source,
        });
      }
    });
  };

  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: () => (
        <Image style={styles.Logo} source={require('../../assets/logo.png')} />
      ),
      headerRight: () => (
        <Image style={styles.Menu} source={require('../../assets/menu.png')} />
      ),
    };
  };

  render() {
    const {checked} = this.state;
    return (
      <View style={styles.ViewAll}>
        <ScrollView>
          <Header
            style={styles.Header}
            containerStyle={{backgroundColor: 'white'}}>
            <TouchableOpacity onPress={this.chooseFile.bind(this)}>
              <Image
                style={styles.Foto}
                source={
                  this.state.filePath
                    ? {uri: this.state.filePath.uri}
                    : require('../../assets/foto.png')
                }
              />
              {/* <Image
                style={styles.Foto}
                source={require('../../assets/foto.png')}
              /> */}
            </TouchableOpacity>
            <Text style={styles.NomeView}>Gabriel do Carmo</Text>
          </Header>
          <View style={styles.View}>
            <Text style={styles.Nome}>Nome:</Text>
            <Input
              style={styles.inputNome}
              placeholder="Ex: Gabriel do Carmo"
              errorStyle={{color: 'red'}}
              errorMessage="Este campo é obrigatório"
            />
            <Text style={styles.UserName}>Username:</Text>
            <Input placeholder="Ex: Gabriel123" />
            <Text style={styles.Sexo}>Sexo:</Text>
            <View>
              <Text style={styles.male}>Masculino</Text>
              <RadioButton
                color={'#86c232'}
                labelHorizontal={true}
                value="1"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({checked: 'first'});
                }}
              />
              <Text style={styles.male}>Feminino</Text>
              <RadioButton
                color={'#86c232'}
                labelHorizontal={true}
                value="0"
                status={checked === 'second' ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({checked: 'second'});
                }}
              />
            </View>
            <Text style={styles.DataNasc}>Data de Nacimento:</Text>
            <DatePicker
              style={{width: 200}}
              date={this.state.data}
              mode="date"
              placeholder="select date"
              format="DD-MM-YYYY"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  textAlign: 'left',
                  top: 10,
                  left: 13,
                },
                dateInput: {
                  textAlign: 'left',
                  top: 10,
                  left: 13,
                  fontSize: 20,
                  color: '#474b4f',
                  fontFamily: 'sans-serif-light',
                  fontWeight: 'bold',
                },
                // ... You can check the source to find the other keys.
              }}
              onDateChange={data => {
                this.setState({data: data});
              }}
            />
            <Text style={styles.Cref}>CREF:</Text>
            <Input
              style={styles.InputCref}
              placeholder="Ex: 123.123-3"
              errorStyle={{color: 'red'}}
              errorMessage="Este campo é obrigatório"
            />
            <Text style={styles.Cpf}>CPF:</Text>
            <Input
              style={styles.InputCpf}
              placeholder="Ex: 123.123.123-01"
              errorStyle={{color: 'red'}}
              errorMessage="Este campo é obrigatório"
            />
            <Text style={styles.Formacao}>Fomação:</Text>
            <Input style={styles.InputFormacao} placeholder="Educação Física" />
            <Text style={styles.Data}>Data de Formação:</Text>
            <DatePicker
              style={{width: 200}}
              date={this.state.date}
              mode="date"
              placeholder="select date"
              format="DD-MM-YYYY"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  textAlign: 'left',
                  top: 10,
                  left: 13,
                },
                dateInput: {
                  textAlign: 'left',
                  top: 10,
                  left: 13,
                  fontSize: 20,
                  color: '#474b4f',
                  fontFamily: 'sans-serif-light',
                  fontWeight: 'bold',
                },
                // ... You can check the source to find the other keys.
              }}
              onDateChange={date => {
                this.setState({date: date});
              }}
            />
            <Text style={styles.Instituicao}>Instituição:</Text>
            <Input style={styles.InputInstituicao} placeholder="Instituição" />
            <Text style={styles.Especialidade}>Especialidade:</Text>
            <Input
              style={styles.InputEspecialidade}
              placeholder="Especialidade"
            />
            <Text style={styles.Publico}>Público Alvo:</Text>
            <Input style={styles.InputPublico} placeholder="Público Alvo" />
            <Text style={styles.Uf}>UF:</Text>
            <Input style={styles.InputUf} placeholder="UF" />
            <Text style={styles.Cidade}>Cidade:</Text>
            <Input style={styles.InputCidade} placeholder="Cidade" />
            <Text style={styles.Endereco}>Endereço:</Text>
            <Input style={styles.InputEndereco} placeholder="Endereço" />
            <Text style={styles.Email}>Email:</Text>
            <Input
              style={styles.InputEmail}
              placeholder="Ex: Gabriel123"
              errorStyle={{color: 'red'}}
              errorMessage="Este campo é obrigatório"
              dataDetectorType={'email'}
            />
            <Text style={styles.Contato}>Contato:</Text>
            <Input style={styles.InputContato} placeholder="Contato" />
            <Text style={styles.RedeSociais}>Rede Sociais:</Text>
            <Input style={styles.InputRedeSociais} placeholder="Rede Sociais" />
            <Text style={styles.Cursos}>Especializações/Cursos:</Text>
            <Input style={styles.InputCursos} placeholder="Cursos" />
            <Text style={styles.Descricao}>Descrição Profissional:</Text>
            <Input style={styles.InputDescricao} placeholder="Descrição" />
            <TouchableOpacity
              style={styles.botaoSalvar}
              onPress={() => {
                this.clicou();
              }}>
              <Text style={styles.TextBotaoSalvar}> Salvar </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
  clicou = () => {
    Alert.alert('Salvo');
  };
}
