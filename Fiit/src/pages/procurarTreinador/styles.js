import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  Logo: {
    width: 125,
    height: 50,
    margin: 10,
  },
  Menu: {
    width: 50,
    height: 50,
    margin: 10,
  },
  Foto: {
    width: 50,
    height: 50,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50,
    top: -10,
  },
  NomeView: {
    top: -10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    marginRight: 50,
  },
  card: {
    backgroundColor: '#fff',
    marginBottom: 10,
    marginLeft: '2%',
    marginRight: '2%',
    width: '96%',
    shadowColor: '#000',
    shadowOpacity: 0.2,
    shadowRadius: 1,
  },
  cardImage: {
    width: '100%',
    height: 200,
    resizeMode: 'cover',
  },
  Footer: {
    position: "absolute",
    bottom: 125,
    justifyContent: 'flex-end',
  },
});

export default styles;
