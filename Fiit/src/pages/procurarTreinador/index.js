import React, { Component } from 'react';
import { TouchableOpacity, Image, Text, View, ScrollView } from 'react-native';
import { Header } from 'react-native-elements';
import { Searchbar } from 'react-native-paper';

import styles from './styles';
import {
  Footer,
  FooterTab,
  Button,
  Icon,
  Content,
  ListItem,
  List,
  Left,
  Right,
  Body,
  Thumbnail,
  Container,
  CardItem,
} from 'native-base';

export default class procurarTreinador extends Component {
  state = {
    firstQuery: '',
  };
  constructor() {
    super();
    this.state = {
      personal: [
        {
          name: 'Treinador 1',
          image: 'https://horadotreino.com.br/wp-content/uploads/2019/07/controle-financeiro-personal-trainer.jpg',
          description: 'Habilidades',
        },
        {
          name: 'Treinador 2',
          image:
            'https://abrilvejasp.files.wordpress.com/2016/11/4965_alyne-soares.jpeg?quality=70&strip=info&w=920',
          description: 'Habilidades',
        },
        {
          name: 'Treinador 3',
          image:
            'https://painel.posestacio.com.br/assets/uploads/81/e5354-personal-ed-fisica.jpg',
          description: 'Habilidades',
        },
        {
          name: 'Treinador 4',
          image:
            'https://abrilvejasp.files.wordpress.com/2016/11/4814__mg_5902.jpeg?quality=70&strip=info&w=920',
          description: 'Habilidades',
        },
      ],
    };
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: () => (
        <Image style={styles.Logo} source={require('../../assets/logo.png')} />
      ),
      headerRight: () => (
        <Image style={styles.Menu} source={require('../../assets/menu.png')} />
      ),
    };
  };

  render() {
    const { firstQuery } = this.state;
    const { navigate } = this.props.navigation;
    return (
      <View style={{ flex: 1 }}>
        <Header
          containerStyle={{
            backgroundColor: 'transparent',
            borderRadius: 4,
            borderWidth: 0.5,
            borderColor: '#d6d7da',
          }}>
          <Image
            style={styles.Foto}
            source={require('../../assets/foto.png')}
          />
          <Text style={styles.NomeView}>Gabriel do Carmo</Text>
        </Header>
        <View>
          <Searchbar
            placeholder="Pesquisar"
            onChangeText={query => { this.setState({ firstQuery: query }); }}
            value={firstQuery}
          />
        </View>
        <View>
          <ScrollView>
            <View style={styles.container}>
              {this.state.personal.map(p => (
                <TouchableOpacity
                  style={styles.card}
                // onPress={() => this.props.navigation.navigate('')} vai direcionar pra tela
                >
                  <Image style={styles.cardImage} source={{ uri: p.image }} />
                  <Text style={styles.cardText}>{p.name}</Text>
                  <Text style={styles.cardText}>{p.description}</Text>
                </TouchableOpacity>
              ))}
            </View>
          </ScrollView>
          <View style={{ flex: 0.1 }}>
            <Footer style={styles.Footer}>
              <FooterTab style={{ backgroundColor: 'white' }}>
                <Button vertical onPress={() => navigate('TelaHomeAluno')}>
                  <Icon name="home" />
                  <Text>Home</Text>
                </Button>
                <Button
                  vertical
                  onPress={() => navigate('TelaProcurarTreinador')}>
                  <Icon name="search" />
                  <Text>Pesquisar</Text>
                </Button>
              </FooterTab>
            </Footer>
          </View>
        </View>
      </View>
    );
  }
}
