import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  Title: {
    fontSize: 70,
    color: '#467D80',
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
  },
});
export default styles;
