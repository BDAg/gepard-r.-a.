import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  TouchableHighlight,
  Alert,
  ScrollView,
  Image,
} from 'react-native';
import styles from './styles';
import { apiServices } from '../../services/api';
import { string } from 'prop-types';

export default class CadastroInicial extends Component {
  static navigationOptions = { header: null };
  constructor(props) {
    super(props);
    this.state = {
      checked: string,
      nome: string,
      email: string,
      senha: string,
      confirmarSenha: string,
    }
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <Image
            style={styles.Image}
            source={require('../../assets/logo.png')}
          />

          <Text style={styles.Title}>Training Assistant</Text>

          <Text style={styles.Cadastro}>Cadastre-se</Text>


          <TextInput
            style={styles.InputNome}
            placeholder="Nome"
            placeholderTextColor="white"
            onChangeText={value => {
              this.setState({ nome: value })
            }}

          />

          <TextInput
            style={styles.InputEmail}
            placeholder="Email"
            placeholderTextColor="white"
            onChangeText={value => {
              this.setState({ email: value })
            }}
          // value = { this.state.email }
          />
          <TextInput
            style={styles.InputSenha}
            secureTextEntry={true}
            placeholder="Senha"
            placeholderTextColor="white"
            onChangeText={value => {
              this.setState({ senha: value })
            }}
          // value= {this.state.senha}
          />
          <TextInput
            style={styles.InputConfirmarSenha}
            secureTextEntry={true}
            placeholder="ConfirmarSenha"
            placeholderTextColor="white"
            onChangeText={value => {
              this.state.confirmarSenha = value;
            }}
          // value = {this.state.confirmarSenha}
          />
          <TouchableHighlight
            style={styles.button}
            onPress={
              () => {
                apiServices.cadastroInicialUser(this.state.nome, this.state.email, this.state.senha, async (err, response) => {
                  this.props.navigation.navigate('TelaCadastroUsuario');
                })
              }
              // this.props.navigation.navigate('TelaHomePersonal')
            }>
            <Text style={styles.buttonText}>Registrar</Text>
          </TouchableHighlight>          
          <TouchableHighlight
            style={styles.button}
            onPress={() => this.props.navigation.navigate('TelaHomePersonal')}
            Personal>
            <Text style={styles.buttonText}>Home Personal</Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}
