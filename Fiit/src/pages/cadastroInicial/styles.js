import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#6b6e70',
    },

    Image: {
        marginTop: 40,
        width: 220,
        height: 80,
    },

    Title: {
        marginTop: 6,
        fontSize: 28,
        color: '#86c232',
        textAlign: 'center'
    },

    Cadastro: {
        marginTop: 45,
        fontSize: 40,
        color: 'white',
        textAlign: 'center',
    },

    InputNome: {
        marginTop: 50,
        width: 300,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        padding: 10,
        borderColor: '#86c232',
        color: 'white',
    },

    InputEmail: {
        marginTop: 30,
        width: 300,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        padding: 10,
        borderColor: '#86c232',
        color: 'white',
    },

    InputSenha: {
        marginTop: 30,
        width: 300,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        padding: 10,
        borderColor: '#86c232',
        color: 'white',
    },

    InputConfirmarSenha: {
        marginTop: 30,
        width: 300,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        padding: 10,
        borderColor: '#86c232',
        color: 'white',
    },

    buttonText: {
        fontSize: 20,
        color: 'white',
        alignSelf: 'center'
    },

    button: {
        borderRadius: 20,
        borderWidth: 0,
        borderBottomColor: 'transparent',
        marginTop: 50,
        marginBottom: 50,
        width: 300,
        height: 40,
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#86c232',
        justifyContent: 'center'
    },

});

export default styles;