import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  Logo: {
    width: 125,
    height: 50,
    margin: 10,
  },
  Menu: {
    width: 50,
    height: 50,
    margin: 10,
  },
  Foto: {
    width: 50,
    height: 50,
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    borderBottomRightRadius: 50,
    top: -10,
  },
  NomeView: {
    top: -10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    marginRight: 150,
  },
  Objetivo: {
    flex: 0.05,
    marginTop: -800,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    color: 'black',
  },
  Robjetivo: {
    marginTop: -20,
    flex: 0.05,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Sedentarismo: {
    color: 'black',
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Rsedentarismo: {
    flex: 0.05,
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Peso: {
    color: 'black',
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Rpeso: {
    flex: 0.05,
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Altura: {
    color: 'black',
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Raltura: {
    flex: 0.05,
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Imc: {
    color: 'black',
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Rimc: {
    flex: 0.05,
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Medidas: {
    color: 'black',
    marginTop: 10,
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'left',
    alignContent: 'flex-start',
  },
  Footer: {
    position: "absolute",
    bottom: 0,
    justifyContent: 'flex-end',
  },
});
export default styles;
