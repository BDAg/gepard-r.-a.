import React, {Component} from 'react';
import {
  Text,
  TextInput,
  View,
  Alert,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  Footer,
  FooterTab,
  Button,
  Icon,
  Content,
  ListItem,
  List,
  Left,
  Right,
  Body,
  Thumbnail,
  Container,
  CardItem,
} from 'native-base';
import {Header, SocialIcon} from 'react-native-elements';

import styles from './styles';
import {Card} from 'react-native-paper';

export default class HomeAluno extends Component {
  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: () => (
        <Image style={styles.Logo} source={require('../../assets/logo.png')} />
      ),
      headerRight: () => (
        <Image style={styles.Menu} source={require('../../assets/menu.png')} />
      ),
    };
  };
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{flex: 1}}>
        <View style={{flex: 0.9}}>
          <Header
            containerStyle={{
              backgroundColor: 'transparent',
              borderRadius: 4,
              borderWidth: 0.5,
              borderColor: '#d6d7da',
            }}>
            <TouchableOpacity
              style={styles.botaoAvatar}
              onPress={() => navigate('TelaCadastroUsuario')}>
              <Image
                style={styles.Foto}
                source={require('../../assets/foto.png')}
              />
            </TouchableOpacity>
            <Text style={styles.NomeView}>Victoria</Text>
          </Header>
        </View>
        <Text style={styles.Objetivo}> Objetivo:</Text>
        <Text style={styles.Robjetivo}> Perder peso</Text>
        <Text style={styles.Sedentarismo}> Nível de sedentarismo</Text>
        <Text style={styles.Rsedentarismo}> Médio</Text>
        <Text style={styles.Peso}> Peso:</Text>
        <Text style={styles.Rpeso}> 64,3 Kg</Text>
        <Text style={styles.Altura}> Altura:</Text>
        <Text style={styles.Raltura}> 1,74</Text>
        <Text style={styles.Imc}> IMC:</Text>
        <Text style={styles.Rimc}> 21,24 - Peso ideal</Text>
        <Text style={styles.Medidas}> Medidas Corporais:</Text>
        <View style={{flex: 0.1}}>
          <Footer style={styles.Footer}>
            <FooterTab style={{backgroundColor: 'white'}}>
              <Button vertical onPress={() => navigate('TelaHomeAluno')}>
                <Icon name="home" />
                <Text>Home</Text>
              </Button>
              <Button
                vertical
                onPress={() => navigate('TelaProcurarTreinador')}>
                <Icon name="search" />
                <Text>Pesquisar</Text>
              </Button>
            </FooterTab>
          </Footer>
        </View>
      </View>
    );
  }
}
