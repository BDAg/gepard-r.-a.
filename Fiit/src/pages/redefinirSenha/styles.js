import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  ViewAll: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  View: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#6b6e70',
  },

  Subtitle: {
    fontSize: 28,
    color: '#FFF',
  },

  imputEmail: {
    marginTop: 90,
    width: 300,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    padding: 10,
    borderColor: '#86c232',
  },

  botaoEnviar: {
    marginTop: 50,
    width: 250,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },

  TextEnviar: {
    color: '#FFF',
  },

  Logo: {
    marginTop: 20,
    width: 220,
    height: 80,
  },
});
export default styles;
