import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
  Image,
  Colors,
} from 'react-native';
import { SocialIcon } from 'react-native-elements';
import styles from './styles';

export default class RecuperarSenha extends Component {
  render() {
    return (
      <View style={styles.ViewAll}>
        <View style={styles.View}>
          <Image
            style={styles.Logo}
            source={require('../../assets/logo.png')}
          />
          <Text style={styles.Subtitle}>Training Assistant</Text>
          <TextInput
            style={styles.imputEmail}
            placeholder="Email"
            placeholderTextColor="white"
          />
          <TouchableOpacity
            style={styles.botaoEnviar}
            onPress={() => {
              this.redefinirSenha();
            }}>
            <Text style={styles.TextEnviar}> Enviar </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  redefinirSenha = () => {
    Alert.alert('Esqueci senha', 'Email enviado');
  };
}
