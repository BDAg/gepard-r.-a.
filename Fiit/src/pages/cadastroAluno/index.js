import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
  Button,
  Alert,
  Icon,
} from 'react-native';
import {Left} from 'native-base';
import {Header, Input} from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import CircleCheckBox, {LABEL_POSITION} from 'react-native-circle-checkbox';
import ImagePicker from 'react-native-image-picker';
import styles from './styles';
import {RadioButton} from 'react-native-paper';

export default class CadastroUsuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '2019-11-13',
      filePath: {},
    };
  }

  chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        {name: 'customOptionKey', title: 'Choose Photo from Custom Option'},
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response;
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.setState({
          filePath: source,
        });
      }
    });
  };

  static navigationOptions = ({navigation}) => {
    return {
      headerTitle: () => (
        <Image style={styles.Logo} source={require('../../assets/logo.png')} />
      ),
      headerRight: () => (
        <Image style={styles.Menu} source={require('../../assets/menu.png')} />
      ),
    };
  };

  render() {
    const {checked} = this.state;
    return (
      <View style={styles.ViewAll}>
        <ScrollView>
          <Header
            style={styles.Header}
            containerStyle={{backgroundColor: 'white'}}>
            <TouchableOpacity onPress={this.chooseFile.bind(this)}>
              <Image
                style={styles.Foto}
                source={
                  this.state.filePath
                    ? {uri: this.state.filePath.uri}
                    : require('../../assets/foto.png')
                }
              />
              {/* <Image
                style={styles.Foto}
                source={require('../../assets/foto.png')}
              /> */}
            </TouchableOpacity>
            <Text style={styles.NomeView}>Victoria</Text>
          </Header>
          <View style={styles.View}>
            <Text style={styles.Nome}>Nome:</Text>
            <Input
              style={styles.inputNome}
              placeholder="Ex: Gabriel do Carmo"
              errorStyle={{color: 'red'}}
              errorMessage="Este campo é obrigatório"
            />
            <Text style={styles.UserName}>Username:</Text>
            <Input placeholder="Ex: Gabriel123" />
            <Text style={styles.Sexo}>Sexo:</Text>
            <View>
              <Text style={styles.male}>Masculino</Text>
              <RadioButton
                color={'#86c232'}
                labelHorizontal={true}
                value="1"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({checked: 'first'});
                }}
              />
              <Text style={styles.male}>Feminino</Text>
              <RadioButton
                color={'#86c232'}
                labelHorizontal={true}
                value="0"
                status={checked === 'second' ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({checked: 'second'});
                }}
              />
            </View>
            <Text style={styles.DataNasc}>Data de Nacimento:</Text>
            <DatePicker
              style={{width: 200}}
              date={this.state.date}
              mode="date"
              placeholder="select date"
              format="DD-MM-YYYY"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  textAlign: 'left',
                  top: 10,
                  left: 13,
                },
                dateInput: {
                  textAlign: 'left',
                  top: 10,
                  left: 13,
                  fontSize: 20,
                  color: '#474b4f',
                  fontFamily: 'sans-serif-light',
                  fontWeight: 'bold',
                },
                // ... You can check the source to find the other keys.
              }}
              onDateChange={date => {
                this.setState({date: date});
              }}
            />
            <Text style={styles.Especialidade}>Peso:</Text>
            <Input style={styles.InputEspecialidade} placeholder="Peso" />
            <Text style={styles.Especialidade}>Altura:</Text>
            <Input style={styles.InputEspecialidade} placeholder="Altura" />
            <Text style={styles.Especialidade}>Objetivo:</Text>
            <Input style={styles.InputEspecialidade} placeholder="Objetivo" />
            <Text style={styles.Endereco}>Endereço:</Text>
            <Input style={styles.InputEndereco} placeholder="Endereço" />
            <Text style={styles.Cidade}>Cidade:</Text>
            <Input style={styles.InputCidade} placeholder="Cidade" />
            <Text style={styles.Uf}>UF:</Text>
            <Input style={styles.InputUf} placeholder="UF" />
            <Text style={styles.Email}>Nível de Sedentarismo:</Text>
            <Input
              style={styles.InputEmail}
              placeholder="Nível de Sedentarismo"
            />
            <Text style={styles.Contato}>Contato:</Text>
            <Input style={styles.InputContato} placeholder="Contato" />
            <Text style={styles.RedeSociais}>Rede Sociais:</Text>
            <Input style={styles.InputRedeSociais} placeholder="Rede Sociais" />
            <Text style={styles.Descricao}>Limitações Físicas:</Text>
            <Input
              style={styles.InputDescricao}
              placeholder="Limitações Físicas"
            />
            <TouchableOpacity
              style={styles.botaoSalvar}
              onPress={() => {
                this.clicou();
              }}>
              <Text style={styles.TextBotaoSalvar}> Salvar </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
  clicou = () => {
    Alert.alert('Salvo');
  };
}