import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#6b6e70',
  },

  Title: {
    flex: 1,
    marginTop: 6,
    fontSize: 28,
    color: '#86c232',
    textAlign: 'center',
  },

  Image: {
    flex: 1,
    marginTop: 50,
    width: 220,
    height: 80,
  },

  RadioButtom: {
    color: '#86c232',
  },

  imputEmail: {
    flex: 1,
    marginTop: 90,
    width: 300,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    padding: 10,
    borderColor: '#86c232',
    color: 'white',
  },

  imputSenha: {
    flex: 1,
    marginTop: 30,
    width: 300,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    padding: 10,
    borderColor: '#86c232',
    color: 'white',
  },

  botaoLogin: {
    flex: 1,
    marginTop: 50,
    width: 300,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#86c232',
    justifyContent: 'center',
    alignItems: 'center',
  },

  botaoEsqueci: {
    width: 250,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },

  TextBotaoEsqueci: {
    fontSize: 12,
    color: 'white',
    marginRight: -160,
  },

  TextBotaoLogin: {
    fontSize: 20,
    color: '#fff',
    fontFamily: 'sans-serif-light',
    fontWeight: 'bold',
    alignContent: 'center',
  },

  EntreCom: {
    fontSize: 15,
    fontFamily: 'sans-serif-light',
    alignContent: 'center',
    color: 'white',
    marginTop: 60,
  },

  Cadastro: {
    flex: 1,
    borderRadius: 20,
    borderWidth: 0,
    marginTop: 20,
    marginBottom: 124,
    width: 300,
    height: 40,
    flexDirection: 'row',
    backgroundColor: 'gray',
    justifyContent: 'center',
  },
});

export default styles;