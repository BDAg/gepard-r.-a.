import React, { Component } from 'react';
import { Text, View, TextInput, TouchableOpacity, Alert, Image, ScrollView, Picker, AsyncStorage } from 'react-native';
import { SocialIcon } from 'react-native-elements';
import { apiServices } from '../../services/api';

import styles from './styles';
import { string } from 'prop-types';

export default class Login extends Component {
  state = {
    checked: 'first',
  };
  constructor(props) {
    super(props);

    this.state = {
      email: string,
      senha: string
    }

    this._storeData = this._storeData.bind(this);

  }

  static navigationOptions = { header: null };

  _storeData = async (token, dados, rota) => {
    try {
      await AsyncStorage.setItem('token', token);
      this.props.navigation.navigate(rota)
      // await AsyncStorage.setItem('dados', dados);
    } catch (error) {
      console.warn(error);
    }
  };

  render() {
    const { checked } = this.state;
    const { navigate } = this.props.navigation;
    let { email, senha } = this.state;
    return (
      <ScrollView>
        <View style={styles.container}>
          <Image
            style={styles.Image}
            source={require('../../assets/logo.png')}
          />

          <Text
            style={styles.Title}>Training Assistant
          </Text>

          <TextInput

            style={styles.imputEmail}
            placeholder="Email"
            type="email"
            placeholderTextColor="white"
            value={this.state.email}
            onChangeText={value => this.setState({ email: value })}
          />

          <TextInput
            style={styles.imputSenha}
            secureTextEntry={true}
            placeholder="Senha"
            placeholderTextColor="white"
            onChangeText={value => this.setState({ senha: value })}
            value={this.state.senha}
          />

          <TouchableOpacity
            style={styles.botaoEsqueci}
            onPress={() => navigate('TelaRecuperarSenha')}>
            <Text style={styles.TextBotaoEsqueci}> Esqueci minha senha </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.botaoLogin}
            onPress={() => {
                apiServices.loginUser(email, senha, async (err, response) => {
                  await this._storeData(response.data['token'], {}, 'TelaHomeAluno');
                });
              
            }}>
            <Text style={styles.TextBotaoLogin}> Login </Text>
          </TouchableOpacity>

          <Text style={styles.EntreCom}> -  Não é cadastrado?  - </Text>

          <SocialIcon
            style={styles.Cadastro}
            title="Cadastre-se"
            button
            type="pencil"
            onPress={() => {
              navigate('TelaCadastroInicial');
            }} />
        </View>
      </ScrollView>
    );
  }
}