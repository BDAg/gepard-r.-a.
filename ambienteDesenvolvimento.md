# Montagem do Ambiente de Desenvolvimento

## Windows 10:

### - VS Code:
   - [Acesse este link] para a instalação do VS Code;
### - NodeJS:
   - Para instalar o NodeJS, [clique aqui];
### - MySQL:
   - [Entre neste link] para baixar todo o pacote MySQL;
### - React Native: <br>
   Para instalar o React Native é necessário complementar a instalação com JDK e Android Studio:
### - JDK:
   - [Acesse aqui] para instalar o JDK;
### - Android Studio:
   - [Entre aqui] e instale Android Studio;
### - Express: <br>
   - No Prompt de comando, digite: <br>
   - $ npm install -g express


## LINUX:

### - VS Code:
   - [Acesse este link] para a instalação do VS Code;
- NodeJS: <br>
   - No Prompt de comando, digite: <br>
      - $ apt install nodejs <br>
   - Depois, verifique a instalação do NodeJS: <br>
      - $ node -v
- NPM:
   - sudo apt-get install openjdk-8-jdk

- MySQL: 
   - [Acesse o link] para baixar o repositório do Mysql na máquina.
   - No prompt de comando, entre na pasta em que o download foi realizado e execute o seguinte comando:
      - $ sudo dpkg -i nome-do-arquivo.deb
   - Atualize as instalações com o comando:
      - $ sudo apt-get update
   - Instale o MySQL:
      - $ sudo apt-get install mysql-server
   - Adicione tambem o MySQL Workbench com o comando:
      - $ sudo apt-get install mysql-workbench-community

- React Native: <br>
   - Para instalar o React Native é necessário complementar a instalação com JDK e Android Studio:

- JDK:
   - Use os comandos:
      - $ sudo add-apt-repository ppa:openjdk-r/ppa
      - $ sudo apt-get update
      - $ sudo apt-get install openjdk-8-jdk

- Android Studio:
   - [Entre no link] para realizar o Download
   - Descompactar o arquivo com o seguinte comando:
      - $ tar -xvf nome-do-arquivo.tar.gz

- Express:
   - Rode o seguinte comando no terminal:
      - $ sudo apt install node-express-generator

[acesse aqui]: https://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html
[acesse este link]: https://code.visualstudio.com/download
[clique aqui]: https://nodejs.org/en/
[entre aqui]: https://developer.android.com/studio
[entre neste link]: https://dev.mysql.com/downloads/installer/
[acesse o link]: https://dev.mysql.com/downloads/repo/apt/
[entre no link]: https://developer.android.com/studio/index.html